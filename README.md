# Image Color Scheme Extractor (ICSE)

A simple PHP color scheme extractor for JPEG/PNG/GIF images

## Installation
- Put `index.php` & `assets/` on your hosting
- *Enjoy!*

## Features
- Representing most visible colors
- Images are temporary (no save task)