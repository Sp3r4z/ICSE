<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Extract image's color palette</title>

  <link rel="stylesheet" href="assets/style.css">

</head>
<body>
  <?php
  $nbColors=5; // number of color to get
	$nbPixels=0;

  function detectColors($image, $num, $real=0) {
    $palette = array();
    $size = getimagesize($image);
    if(!$size) {
      return FALSE;
    }
    switch($size['mime']) {
      case 'image/jpeg': $img = imagecreatefromjpeg($image); break;
      case 'image/png': $img = imagecreatefrompng($image); break;
      case 'image/gif': $img = imagecreatefromgif($image); break;
      default: return FALSE;
    }
    if(!$img) {
      return FALSE;
    }
    if(!$real) {
      if($size[0]>=2000 || $size[1]>=2000) $level=25;
      else if($size[0]>=5000 || $size[1]>=5000) $level=75;
      else $level=15;
    }
    else $level=1;

    for($i = 0; $i < $size[0]; $i += $level) {
      for($j = 0; $j < $size[1]; $j += $level) {
        $thisColor = imagecolorat($img, $i, $j);
        $rgb = imagecolorsforindex($img, $thisColor);
        $color = sprintf('%02X%02X%02X', (round(round(($rgb['red'] / 0x33)) * 0x33)), round(round(($rgb['green'] / 0x33)) * 0x33), round(round(($rgb['blue'] / 0x33)) * 0x33));
        $palette[$color] = isset($palette[$color]) ? ++$palette[$color] : 1;
      }
    }
    arsort($palette);

		global $nbPixels;
		$nbPixels=array_sum($palette);

    $nbPalette=count($palette);
    if($nbPalette<$num)$num=$nbPalette;
    return array_slice($palette, 0, $num,TRUE);
  }

  function getBase64($file,$type) {
    $data = file_get_contents($file);
    $base64 = 'data:'.$type.';base64,'.base64_encode($data);
    return $base64;
  }

  function footer() {
    echo '<footer>Project by <a href="https://mastodon.xyz/@Sp3r4z" rel="me">@Sp3r4z</a> — Source: <a href="https://framagit.org/Sp3r4z/ICSE">ICSE on Framagit</a></footer>';
  }

  ?>

  <?php
  if(isset($_POST["submit"])) {
    $fileName=sha1($_FILES["fileToUpload"]["name"]);
    $imageFileType = mime_content_type($_FILES["fileToUpload"]["tmp_name"]);

    if ($_FILES["fileToUpload"]["size"] > 10000000) {
      echo "Sorry, your file is too large.";
    }
    else if(!preg_match('/image\/[jpeg|png]/i',$imageFileType)) {
      echo "Sorry, only JPG/JPEG, PNG & TIFF files are allowed.";
    }
    else {
      move_uploaded_file($_FILES["fileToUpload"]["tmp_name"],$fileName);
      $colors=detectColors($fileName, $nbColors);
      ?>

      <img src="<?=getBase64($fileName,$imageFileType)?>" alt="">
      <div class="colors">
        <?php
        foreach ($colors as $key => $value) {
					echo "<span style='background-color:#$key;color:white' title='#$key - ".round($value*100/$nbPixels)."%'></span>";
        }

        ?>
      </div>

      <?php
      footer();
      unlink($fileName); // auto-delete file
    }
  }
  else {
    ?>
    <form action="index.php" method="post" enctype="multipart/form-data">
      Select image to upload:
      <input type="file" name="fileToUpload" id="fileToUpload">
      <input type="submit" value="Upload Image" name="submit">
    </form>
    <?php
    footer();
  }
  ?>
</body>
</html>
